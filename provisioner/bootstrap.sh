sudo apt-get update

sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password vagrant'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password vagrant'
if [ ! -f /var/log/aptdoneupdate ]; then
    sudo apt-get update
    sudo touch /var/log/aptdoneupdate
fi

sudo apt-get -y install vim build-essential curl apache2 mysql-server-5.5 git-core beanstalkd
sudo apt-get -y install libapache2-mod-php5 php5 php5-dev php5-gd  php-pear  php5-curl php5-mysql php5-cli libapache2-mod-suphp

# Set timezone
echo "Asia/Ho_Chi_Minh" | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata


# Setup database
if [ ! -f /var/log/databasesetup ];
then
    sudo sed -e 's#127.0.0.1#0.0.0.0#g' -i '/etc/mysql/my.cnf'
    sudo service mysql restart

    echo "DROP DATABASE IF EXISTS test" | mysql -uroot -pvagrant
    echo "CREATE USER 'vagrant'@'%' IDENTIFIED BY 'vagrant'" | mysql -uroot -pvagrant
    echo "CREATE DATABASE devdb" | mysql -uroot -pvagrant
    echo "GRANT ALL ON devdb.* TO 'vagrant'@'%'" | mysql -uroot -pvagrant
    echo "flush privileges" | mysql -uroot -pvagrant

    sudo touch /var/log/databasesetup

    if [ -f /var/sqldump/database.sql ];
    then
        mysql -uroot -pvagrant vagrant < /var/sqldump/database.sql
    fi
fi

cd /usr/local/src/



# Apache changes
if [ ! -f /var/log/webserversetup ];
then
    echo "ServerName localhost" | sudo tee /etc/apache2/httpd.conf > /dev/null
    sudo a2enmod rewrite
    sudo sed -i '/AllowOverride None/c AllowOverride All' /etc/apache2/sites-available/default

    sudo touch /var/log/webserversetup
fi

# Install xdebug
if [ ! -f /var/log/xdebugsetup ];
then
    sudo apt-get -y install php5-xdebug
    XDEBUG_LOCATION=$(find / -name 'xdebug.so' 2> /dev/null)

    sudo touch /var/log/xdebugsetup
fi


# Configure PHP
if [ ! -f /var/log/phpsetup ];
then
    sudo sed -i '/;sendmail_path =/c sendmail_path = "/opt/vagrant_ruby/bin/catchmail"' /etc/php5/apache2/php.ini
    sudo sed -i '/display_errors = Off/c display_errors = On' /etc/php5/apache2/php.ini
    sudo sed -i '/error_reporting = E_ALL & ~E_DEPRECATED/c error_reporting = E_ALL | E_STRICT' /etc/php5/apache2/php.ini
    sudo sed -i '/html_errors = Off/c html_errors = On' /etc/php5/apache2/php.ini
    echo "zend_extension='$XDEBUG_LOCATION'" | sudo tee -a /etc/php5/apache2/php.ini > /dev/null

    sudo touch /var/log/phpsetup
fi

# Setup PHP Adminer for MySQL/MariaDB
if [ ! -f /var/log/phpadminer ];
then
    sudo rm /var/www/html/adminer.php
    wget http://sourceforge.net/projects/adminer/files/latest/download?source=files -O adminer.php
    sudo mv adminer.php /var/www/html/
    sudo touch /var/log/phpadminer
fi


# Install Composer
if [ ! -f /usr/bin/composer ];
then
    cd /opt/
    sudo curl -s https://getcomposer.org/installer | sudo php
    sudo mv composer.phar /usr/bin/composer
fi

# Install PHPUnit
if [ ! -f /usr/bin/phpunit ];
then
    cd /tmp
    wget https://phar.phpunit.de/phpunit.phar
    chmod +x phpunit.phar
    sudo mv phpunit.phar /usr/bin/phpunit
    phpunit --version
fi

# Install Mailcatcher
if [ ! -f /var/log/mailcatchersetup ];
then
    sudo apt-get install libsqlite3-dev
    sudo /opt/vagrant_ruby/bin/gem install mailcatcher
    mailcatcher --ip=0.0.0.0

    sudo touch /var/log/mailcatchersetup
fi

# Install and setup Java
if [ ! -f /var/log/setupjava ]; then
    cd /tmp
    curl https://s3-ap-southeast-1.amazonaws.com/tambinh-downloads/jdk-7-linux-x64.tar.gz -o jdk-7-linux-x64.tar.gz
    tar -xvf jdk-7-linux-x64.tar.gz

    sudo mkdir -p /usr/lib/jvm
    sudo mv ./jdk1.7.* /usr/lib/jvm/jdk1.7.0

    sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.7.0/bin/java" 1
    sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.7.0/bin/javac" 1
    sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.7.0/bin/javaws" 1

    sudo chmod a+x /usr/bin/java 
    sudo chmod a+x /usr/bin/javac 
    sudo chmod a+x /usr/bin/javaws
    sudo chown -R root:root /usr/lib/jvm/jdk1.7.0

    rm jdk-7-linux-x64.tar.gz
    
    sudo touch /var/log/setupjava
    java -version
fi

# Upgrade to php 5.4
if [ ! -f /var/log/upgrdaephp54 ]; then
    sudo apt-get -y install software-properties-common python-software-properties
    sudo add-apt-repository -y ppa:ondrej/php5-oldstable
    sudo apt-get update; sudo apt-get install php5 php5-cli

    sudo touch /var/log/upgrdaephp54
fi


# Install Phalcon PHP
if [ ! -f /var/log/phalcondoneinstall ];
then
    sudo apt-get install -y php5-dev libpcre3-dev gcc make php5-mysql
    cd /opt
    sudo git clone --depth=1 git://github.com/phalcon/cphalcon.git
    cd cphalcon/build
    sudo ./install    
    sudo echo 'extension=phalcon.so' > /etc/php5/mods-available/phalcon.ini
    sudo php5enmod phalcon
    sudo touch /var/log/phalcondoneinstall
fi

# Install Phalcon Dev-Tools
if [ ! -f /usr/bin/phalcon ];
then
    cd /opt
    sudo git clone https://github.com/phalcon/phalcon-devtools
    cd phalcon-devtools
    sudo ln -s /opt/phalcon-devtools/phalcon.php /usr/bin/phalcon
    sudo chmod ugo+x /usr/bin/phalcon
fi

# setup hosts
 sudo cp /vagrant/provisioner/geekup.pm.conf /etc/apache2/sites-available/geekup.pm.conf
 sudo a2ensite geekup.pm.conf

# enable apache modules
sudo a2enmod rewrite
sudo a2enmod setenvif
sudo a2enmod autoindex
sudo a2enmod deflate
sudo a2enmod filter
sudo a2enmod headers
sudo a2enmod expires


# Make sure things are up and running as they should be
mailcatcher --http-ip=192.168.33.10
sudo service apache2 restart